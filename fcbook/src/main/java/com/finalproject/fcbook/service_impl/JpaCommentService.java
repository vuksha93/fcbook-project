package com.finalproject.fcbook.service_impl;

import javax.transaction.Transactional;

import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.repository.CommentRepository;
import com.finalproject.fcbook.service.CommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class JpaCommentService implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment findOne(int id) {
        return commentRepository.findById(id).get();
    }

    @Override
    public Comment remove(int id) {
        Comment comment = findOne(id);
        commentRepository.deleteById(id);
        return comment;
    }

}
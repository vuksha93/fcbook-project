package com.finalproject.fcbook.service_impl;

import java.util.List;

import javax.transaction.Transactional;

import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.Post;
import com.finalproject.fcbook.repository.PostRepository;
import com.finalproject.fcbook.service.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class JpaPostService implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public Post save(Post post) {
        return postRepository.save(post);
    }

    @Override
    public Post findOne(int id) {
        return postRepository.findById(id).get();
    }

    @Override
    public Post remove(int id) {
        Post post = postRepository.getOne(id);
        postRepository.delete(post);
        return post;
    }

    @Override
    public List<Comment> getPostComments(int id) {
        return postRepository.getPostComments(id);
    }

    @Override
    public List<Post> getFeed(int id) {
        return postRepository.getFeed(id);
    }

    @Override
    public List<Post> getUserPosts(int id) {
        return postRepository.getUserPosts(id);
    }

}
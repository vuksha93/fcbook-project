package com.finalproject.fcbook.service_impl;

import javax.transaction.Transactional;

import com.finalproject.fcbook.model.About;
import com.finalproject.fcbook.repository.AboutRepository;
import com.finalproject.fcbook.service.AboutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class JpaAboutService implements AboutService {

    @Autowired
    private AboutRepository aboutRepository;

    @Override
    public About save(About about) {
        return aboutRepository.save(about);
    }

}
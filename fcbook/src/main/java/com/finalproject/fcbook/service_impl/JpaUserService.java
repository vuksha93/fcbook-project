package com.finalproject.fcbook.service_impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.finalproject.fcbook.dto.AuthRequest;
import com.finalproject.fcbook.dto.UserDTO;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.repository.UserRepository;
import com.finalproject.fcbook.service.UserService;
import com.finalproject.fcbook.support.UserToUserDTO;
import com.finalproject.fcbook.util.JwtUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class JpaUserService implements UserService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserToUserDTO toUserDTO;

    @Override
    public User findOne(int id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User remove(int id) {
        User user = findOne(id);
        for (User friend : user.getFriends()) {
            friend.getFriends().remove(user);
            save(friend);
        }
        userRepository.delete(user);
        return user;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public String generateToken(AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(),
                    authRequest.getPassword(), new ArrayList<>()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Invalid username or password!");
        }

        return jwtUtil.generateToken(authRequest.getUsername());
    }

    @Override
    public List<UserDTO> getFriendsByUserID(int id, int requestorId) {
        List<Object[]> friends = userRepository.getFriendsByUserID(id, requestorId);
        List<UserDTO> userDtos = new ArrayList<>();
        for (Object[] objects : friends) {
            User user = (User) objects[0];
            boolean isFriend = (boolean) objects[1];
            UserDTO userDTO = toUserDTO.convert(user);
            userDTO.setIsFriend(isFriend);
            userDtos.add(userDTO);
        }

        return userDtos;
    }

    @Override
    public List<UserDTO> search(String name, int requestorId) {
        List<Object[]> foundUsers = userRepository.search(name, requestorId);
        List<UserDTO> userDTOs = new ArrayList<>();
        for (Object[] objects : foundUsers) {
            User user = (User) objects[0];
            boolean isFriend = (boolean) objects[1];
            UserDTO userDTO = toUserDTO.convert(user);
            userDTO.setIsFriend(isFriend);
            userDTOs.add(userDTO);
        }
        return userDTOs;
    }

    @Override
    public UserDTO findUserByID(int id, int requestorID) {
        List<Object[]> foundUser = userRepository.findUserById(id, requestorID);
        UserDTO userDTO = new UserDTO();
        for (Object[] objects : foundUser) {
            User user = (User) objects[0];
            boolean isFriend = (boolean) objects[1];
            userDTO = toUserDTO.convert(user);
            userDTO.setIsFriend(isFriend);
        }

        return userDTO;
    }

}
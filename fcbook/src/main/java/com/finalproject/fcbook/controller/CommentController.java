package com.finalproject.fcbook.controller;

import java.security.Principal;

import com.finalproject.fcbook.dto.CommentCreationDTO;
import com.finalproject.fcbook.dto.CommentDTO;
import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.CommentService;
import com.finalproject.fcbook.service.UserService;
import com.finalproject.fcbook.support.CommentToCommentDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/comments")
@CrossOrigin
public class CommentController {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentToCommentDTO toCommentDTO;

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public CommentDTO deleteComment(Principal principal, @PathVariable int id) throws Exception {
        User user = userService.findByUsername(principal.getName());
        Comment comment = commentService.findOne(id);

        if (comment != null
                && (comment.getUser().getId() == user.getId() || comment.getPost().getUser().getId() == user.getId())) {
            commentService.remove(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return toCommentDTO.convert(comment);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{id}")
    public CommentDTO editComment(@PathVariable int id, Principal principal,
            @RequestBody CommentCreationDTO commentCreationDTO) throws Exception {
        User user = userService.findByUsername(principal.getName());
        Comment comment = commentService.findOne(id);

        if (comment != null && comment.getUser().getId() == user.getId()) {
            comment.setContent(commentCreationDTO.getContent());
            commentService.save(comment);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return toCommentDTO.convert(comment);
    }

}
package com.finalproject.fcbook.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.AuthRequest;
import com.finalproject.fcbook.dto.UserDTO;
import com.finalproject.fcbook.dto.UserRegistrationDTO;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.UserService;
import com.finalproject.fcbook.support.UserRegistrationDTOToUser;
import com.finalproject.fcbook.support.UserToUserDTO;
import com.finalproject.fcbook.util.JwtUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private UserRegistrationDTOToUser userRegistrationDTOtoUser;

    @Autowired
    private UserToUserDTO toUserDTO;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private UserService userService;

    private static String imageDirectory = System.getProperty("user.dir") + "/image/";

    @PostMapping(value = "/image", produces = { MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE })
    public ResponseEntity<?> uploadImage(@RequestParam("imageFile") MultipartFile file) {
        makeDirectoryIfNotExist(imageDirectory);
        Path fileNamePath = Paths.get(imageDirectory);
        try {
            Files.write(fileNamePath, file.getBytes());
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (IOException ex) {
            return new ResponseEntity<>("Image is not uploaded", HttpStatus.BAD_REQUEST);
        }
    }

    private void makeDirectoryIfNotExist(String imageDirectory) {
        File directory = new File(imageDirectory);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        return userService.generateToken(authRequest);
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/registration")
    public void addUser(@RequestBody UserRegistrationDTO userRegistrationDTO) throws Exception {
        List<User> users = userService.findAll();
        if (userRegistrationDTO.getPassword().equals(userRegistrationDTO.getRepeatedPassword())) {
            User user = userRegistrationDTOtoUser.convert(userRegistrationDTO);
            for (User usr : users) {
                if (user.getUsername().equals(usr.getUsername())
                        || user.getAbout().getEmail().equals(usr.getAbout().getEmail())) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                }
            }
            user.setPassword(encoder.encode(user.getPassword()));
            user = userService.save(user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Passwords don't match!");
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/user")
    public UserDTO getLoggedUser(Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());
        return toUserDTO.convert(user);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    public UserDTO getUser(@PathVariable int id, Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());
        UserDTO foundUser = userService.findUserByID(id, user.getId());
        if (foundUser == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id:" + id + " is not found!");
        }

        return foundUser;
    }

    @GetMapping("/user/{id}")
    public UserDTO getUserByID(@PathVariable int id, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        return userService.findUserByID(id, user.getId());
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping
    public List<UserDTO> getUsers() {
        List<User> users = userService.findAll();
        return toUserDTO.convert(users);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/search")
    public List<UserDTO> search(@RequestParam(required = true) String name, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        List<UserDTO> foundUsers = userService.search(name, user.getId());

        return foundUsers;
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping
    public String editUser(@RequestBody UserRegistrationDTO userRegistrationDTO, Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());

        if (user != null && userRegistrationDTO.getPassword().equals(userRegistrationDTO.getRepeatedPassword())) {
            userRegistrationDTO.setPassword(encoder.encode(user.getPassword()));
            user.setUsername(userRegistrationDTO.getUsername());
            user.setPassword(userRegistrationDTO.getPassword());
            user.getAbout().setEmail(userRegistrationDTO.getEmail());
            user.getAbout().setFirstName(userRegistrationDTO.getFirstName());
            user.getAbout().setLastName(userRegistrationDTO.getLastName());
            user.getAbout().setYearOfBirth(userRegistrationDTO.getYearOfBirth());
            user = userService.save(user);
            return jwtUtil.generateToken(user.getUsername());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id, Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());

        if (user != null) {
            userService.remove(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id:" + id + " is not found!");
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PostMapping("/addfriend/{id}")
    public void addFriend(@PathVariable int id, Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());
        User friend = userService.findOne(id);

        if (friend != null && user != null) {
            user.addFriend(friend);
            userService.save(user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/deletefriend/{friendID}")
    public void deleteFriend(@PathVariable int friendID, Principal principal) throws Exception {
        User user = userService.findByUsername(principal.getName());
        User friend = userService.findOne(friendID);

        if (user != null && friend != null) {
            user.unfriend(friend);
            userService.save(user);
            userService.save(friend);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}/friends")
    public List<UserDTO> getFriendsByID(@PathVariable int id, Principal principal) throws Exception {
        List<UserDTO> friends = new ArrayList<>();
        User friend = userService.findOne(id);
        User user = userService.findByUsername(principal.getName());

        if (friend != null) {
            friends = userService.getFriendsByUserID(id, user.getId());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Friend with id:" + id + " is not found!");
        }

        return friends;
    }

}
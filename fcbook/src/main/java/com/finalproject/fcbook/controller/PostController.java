package com.finalproject.fcbook.controller;

import java.security.Principal;
import java.util.List;

import com.finalproject.fcbook.dto.CommentCreationDTO;
import com.finalproject.fcbook.dto.CommentDTO;
import com.finalproject.fcbook.dto.PostCreationDTO;
import com.finalproject.fcbook.dto.PostDTO;
import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.Post;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.CommentService;
import com.finalproject.fcbook.service.PostService;
import com.finalproject.fcbook.service.UserService;
import com.finalproject.fcbook.support.CommentCreationDTOToComment;
import com.finalproject.fcbook.support.CommentToCommentDTO;
import com.finalproject.fcbook.support.PostCreationDTOToPost;
import com.finalproject.fcbook.support.PostToPostDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/posts")
@CrossOrigin
public class PostController {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private PostToPostDTO postToPostDTO;

    @Autowired
    private PostCreationDTOToPost postCreationDTOToPost;

    @Autowired
    private CommentToCommentDTO commentToCommentDTO;

    @Autowired
    private CommentCreationDTOToComment commentCreationDTOToComment;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public PostDTO addPost(Principal principal, @RequestBody PostCreationDTO postCreationDTO) throws Exception {
        User user = userService.findByUsername(principal.getName());
        Post post;

        if (user != null) {
            post = postCreationDTOToPost.convert(postCreationDTO);
            post.setUser(user);
            postService.save(post);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return postToPostDTO.convert(post);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{id}")
    public void editPost(Principal principal, @RequestBody PostCreationDTO postCreationDTO, @PathVariable int id)
            throws Exception {
        User user = userService.findByUsername(principal.getName());
        Post post = postService.findOne(id);

        if (post != null && post.getUser().getId() == user.getId()) {
            post.setContent(postCreationDTO.getContent());
            postService.save(post);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deletePost(Principal principal, @PathVariable int id) throws Exception {
        User user = userService.findByUsername(principal.getName());
        Post post = postService.findOne(id);

        if (user != null && post != null && user.getPosts().contains(post)) {
            postService.remove(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/feed")
    public List<PostDTO> getFriendPosts(Principal principal) {
        User user = userService.findByUsername(principal.getName());
        return postToPostDTO.convert(postService.getFeed(user.getId()));
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{userID}")
    public List<PostDTO> getUserPosts(@PathVariable int userID) {
        List<Post> posts = postService.getUserPosts(userID);
        return postToPostDTO.convert(posts);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}/comments")
    public List<CommentDTO> getPostComments(@PathVariable int id) {
        List<Comment> comments = postService.getPostComments(id);
        return commentToCommentDTO.convert(comments);
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping({ "/{postID}/addcomment", "/{postID}/addcomment/{commentID}" })
    public CommentDTO addComment(@PathVariable int postID, @PathVariable(required = false) Integer commentID,
            @RequestBody CommentCreationDTO commentCreationDTO, Principal principal) throws Exception {

        Post post = postService.findOne(postID);
        User user = userService.findByUsername(principal.getName());
        Comment newComment = new Comment();

        if (post != null) {
            newComment = commentCreationDTOToComment.convert(commentCreationDTO);
            newComment.setPost(post);
            newComment.setUser(user);
            if (commentID != null) {
                Comment comment = commentService.findOne(commentID.intValue());
                if (comment != null) {
                    comment.addComment(newComment);
                }
            }
            commentService.save(newComment);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return commentToCommentDTO.convert(newComment);

    }

}
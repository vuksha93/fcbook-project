package com.finalproject.fcbook.util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.finalproject.fcbook.model.About;
import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.Post;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.AboutService;
import com.finalproject.fcbook.service.CommentService;
import com.finalproject.fcbook.service.PostService;
import com.finalproject.fcbook.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class TestData {

    final int DATA_NUMBER = 5;

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @Autowired
    private AboutService aboutService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private PasswordEncoder encoder;

    @PostConstruct
    public void addData() {
        List<User> users = new ArrayList<>();

        for (int i = 1; i < DATA_NUMBER; i++) {
            User user = new User();
            user.setUsername("username" + i);
            user.setPassword(encoder.encode("password" + i));
            user.setImgPath("");
            user = userService.save(user);
            users.add(user);
            About about = new About();

            about.setEmail("email" + i + ".com");
            about.setFirstName("firstName" + i);
            about.setLastName("lastName" + i);
            about.setYearOfBirth(1990 + i);
            about.setUser(user);

            aboutService.save(about);
            user.setAbout(about);
        }

        for (User user : users) {
            for (User friend : users) {
                if (user != friend) {
                    user.addFriend(friend);
                }
            }
            userService.save(user);
        }

        for (int j = 0; j < DATA_NUMBER * 3; j++) {
            User postAuthor = userService.findOne(j % (DATA_NUMBER - 1) + 1);
            Post post = new Post();
            post.setContent("post" + (j + 1));
            post.setCreationTime(LocalDateTime.now());
            post.setUser(postAuthor);
            postAuthor.addPost(post);
            postService.save(post);
            for (int k = 1; k < DATA_NUMBER - 1; k++) {
                User commentAuthor = userService.findOne(k % DATA_NUMBER);
                Comment comment = new Comment();
                comment.setContent("comment" + k);
                comment.setPost(post);
                comment.setUser(commentAuthor);
                commentService.save(comment);
                post.addComment(comment);
                commentAuthor.addComment(comment);
                if (k != 1) {
                    for (int l = 2; l < DATA_NUMBER; l += 2) {
                        User replyAuthor = userService.findOne(l % DATA_NUMBER);
                        Comment reply = new Comment();
                        reply.setContent("reply");
                        reply.setUser(replyAuthor);
                        comment.addComment(reply);
                        reply.setParentComment(comment);
                        commentService.save(reply);
                    }
                    commentService.save(comment);
                }
            }
        }

    }

}
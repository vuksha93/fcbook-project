package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.CommentDTO;
import com.finalproject.fcbook.model.Comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentToCommentDTO implements Converter<Comment, CommentDTO> {

    @Autowired
    private UserToUserDTO toUserDTO;

    @Autowired
    private PostToPostDTO toPostDTO;

    @Override
    public CommentDTO convert(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();

        commentDTO.setId(comment.getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setUserDTO(toUserDTO.convert(comment.getUser()));
        commentDTO.setPostDTO(toPostDTO.convert(comment.getPost()));
        if (comment.getParentComment() != null) {
            commentDTO.setParentCommentID(comment.getParentComment().getId());
        }

        return commentDTO;
    }

    public List<CommentDTO> convert(List<Comment> comments) {
        List<CommentDTO> commentDTOs = new ArrayList<>();

        for (Comment comment : comments) {
            commentDTOs.add(convert(comment));
        }

        return commentDTOs;
    }

}
package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.UserRegistrationDTO;
import com.finalproject.fcbook.model.User;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserRegistrationDTO implements Converter<User, UserRegistrationDTO> {

    @Override
    public UserRegistrationDTO convert(User user) {
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();

        userRegistrationDTO.setId(user.getId());
        userRegistrationDTO.setUsername(user.getUsername());
        userRegistrationDTO.setPassword(user.getPassword());
        userRegistrationDTO.setRepeatedPassword(user.getPassword());
        userRegistrationDTO.setEmail(user.getAbout().getEmail());
        userRegistrationDTO.setFirstName(user.getAbout().getFirstName());
        userRegistrationDTO.setLastName(user.getAbout().getLastName());
        userRegistrationDTO.setYearOfBirth(user.getAbout().getYearOfBirth());

        return userRegistrationDTO;
    }

    public List<UserRegistrationDTO> convert(List<User> users) {
        List<UserRegistrationDTO> userRegistrationDTOs = new ArrayList<>();

        for (User user : users) {
            userRegistrationDTOs.add(convert(user));
        }

        return userRegistrationDTOs;
    }

}
package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.PostDTO;
import com.finalproject.fcbook.model.Post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostDTOToPost implements Converter<PostDTO, Post> {

    @Autowired
    private UserDTOToUser toUser;

    @Override
    public Post convert(PostDTO postDTO) {
        Post post = new Post();

        post.setId(postDTO.getId());
        post.setContent(postDTO.getContent());
        post.setUser(toUser.convert(postDTO.getUserDTO()));
        post.setCreationTime(postDTO.getCreationTime());

        return post;
    }

    public List<Post> convert(List<PostDTO> postDTOs) {
        List<Post> posts = new ArrayList<>();

        for (PostDTO postDTO : postDTOs) {
            posts.add(convert(postDTO));
        }

        return posts;
    }

}
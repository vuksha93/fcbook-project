package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.UserRegistrationDTO;
import com.finalproject.fcbook.model.About;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.AboutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserRegistrationDTOToUser implements Converter<UserRegistrationDTO, User> {

    @Autowired
    private AboutService aboutService;

    @Override
    public User convert(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();

        user.setId(userRegistrationDTO.getId());
        user.setUsername(userRegistrationDTO.getUsername());
        user.setPassword(userRegistrationDTO.getPassword());
        About about = new About();

        about.setId(0);
        about.setEmail(userRegistrationDTO.getEmail());
        about.setFirstName(userRegistrationDTO.getFirstName());
        about.setLastName(userRegistrationDTO.getLastName());
        about.setYearOfBirth(userRegistrationDTO.getYearOfBirth());

        aboutService.save(about);
        user.setAbout(about);
        about.setUser(user);

        return user;
    }

    public List<User> convert(List<UserRegistrationDTO> userRegistrationDTOs) {
        List<User> users = new ArrayList<>();

        for (UserRegistrationDTO userRegistrationDTO : userRegistrationDTOs) {
            users.add(convert(userRegistrationDTO));
        }

        return users;
    }

}
package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.CommentCreationDTO;
import com.finalproject.fcbook.model.Comment;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentCreationDTOToComment implements Converter<CommentCreationDTO, Comment> {

    @Override
    public Comment convert(CommentCreationDTO commentCreationDTO) {
        Comment comment = new Comment();

        comment.setId(commentCreationDTO.getId());
        comment.setContent(commentCreationDTO.getContent());

        return comment;
    }

    public List<Comment> convert(List<CommentCreationDTO> commentCreationDTOs) {
        List<Comment> comments = new ArrayList<>();

        for (CommentCreationDTO commentCreationDTO : commentCreationDTOs) {
            comments.add(convert(commentCreationDTO));
        }

        return comments;
    }

}
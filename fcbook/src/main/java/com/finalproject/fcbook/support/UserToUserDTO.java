package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.UserDTO;
import com.finalproject.fcbook.model.User;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDTO implements Converter<User, UserDTO> {

    @Override
    public UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setFirstName(user.getAbout().getFirstName());
        userDTO.setLastName(user.getAbout().getLastName());
        userDTO.setEmail(user.getAbout().getEmail());
        userDTO.setYearOfBirth(user.getAbout().getYearOfBirth());

        return userDTO;
    }

    public List<UserDTO> convert(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();

        for (User user : users) {
            userDTOs.add(convert(user));
        }

        return userDTOs;
    }

}
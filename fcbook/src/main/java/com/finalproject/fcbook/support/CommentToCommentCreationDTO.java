package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.CommentCreationDTO;
import com.finalproject.fcbook.model.Comment;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentToCommentCreationDTO implements Converter<Comment, CommentCreationDTO> {

    @Override
    public CommentCreationDTO convert(Comment comment) {
        CommentCreationDTO commentCreationDTO = new CommentCreationDTO();

        commentCreationDTO.setId(comment.getId());
        commentCreationDTO.setContent(comment.getContent());

        return commentCreationDTO;
    }

    public List<CommentCreationDTO> convert(List<Comment> comments) {
        List<CommentCreationDTO> commentCreationDTOs = new ArrayList<>();

        for (Comment comment : comments) {
            commentCreationDTOs.add(convert(comment));
        }

        return commentCreationDTOs;
    }

}
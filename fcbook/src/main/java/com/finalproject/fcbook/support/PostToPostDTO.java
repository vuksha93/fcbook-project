package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.PostDTO;
import com.finalproject.fcbook.model.Post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostToPostDTO implements Converter<Post, PostDTO> {

    @Autowired
    private UserToUserDTO toUserDTO;

    @Override
    public PostDTO convert(Post post) {
        PostDTO postDTO = new PostDTO();

        postDTO.setId(post.getId());
        postDTO.setContent(post.getContent());
        postDTO.setUserDTO(toUserDTO.convert(post.getUser()));
        postDTO.setCreationTime(post.getCreationTime());

        return postDTO;
    }

    public List<PostDTO> convert(List<Post> posts) {
        List<PostDTO> postDTOs = new ArrayList<>();

        for (Post post : posts) {
            postDTOs.add(convert(post));
        }

        return postDTOs;
    }

}
package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.PostCreationDTO;
import com.finalproject.fcbook.model.Post;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostCreationDTOToPost implements Converter<PostCreationDTO, Post> {

    @Override
    public Post convert(PostCreationDTO postCreationDTO) {
        Post post = new Post();

        post.setId(postCreationDTO.getId());
        post.setContent(postCreationDTO.getContent());
        post.setCreationTime(postCreationDTO.getCreationTime());

        return post;
    }

    public List<Post> convert(List<PostCreationDTO> postCreationDTOs) {
        List<Post> posts = new ArrayList<>();

        for (PostCreationDTO postCreationDTO : postCreationDTOs) {
            posts.add(convert(postCreationDTO));
        }

        return posts;
    }

}
package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.CommentDTO;
import com.finalproject.fcbook.model.Comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommentDTOToComment implements Converter<CommentDTO, Comment> {

    @Autowired
    private UserDTOToUser toUser;

    @Autowired
    private PostDTOToPost toPost;

    @Override
    public Comment convert(CommentDTO commentDTO) {
        Comment comment = new Comment();

        comment.setId(commentDTO.getId());
        comment.setContent(commentDTO.getContent());
        comment.setUser(toUser.convert(commentDTO.getUserDTO()));
        comment.setPost(toPost.convert(commentDTO.getPostDTO()));

        return comment;
    }

    public List<Comment> convert(List<CommentDTO> commentDTOs) {
        List<Comment> comments = new ArrayList<>();

        for (CommentDTO commentDTO : commentDTOs) {
            comments.add(convert(commentDTO));
        }

        return comments;
    }

}
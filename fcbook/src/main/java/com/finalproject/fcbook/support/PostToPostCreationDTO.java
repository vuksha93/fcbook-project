package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.PostCreationDTO;
import com.finalproject.fcbook.model.Post;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostToPostCreationDTO implements Converter<Post, PostCreationDTO> {

    @Override
    public PostCreationDTO convert(Post post) {
        PostCreationDTO postCreationDTO = new PostCreationDTO();

        postCreationDTO.setId(post.getId());
        postCreationDTO.setContent(post.getContent());
        postCreationDTO.setCreationTime(post.getCreationTime());

        return postCreationDTO;
    }

    public List<PostCreationDTO> convert(List<Post> posts) {
        List<PostCreationDTO> postCreationDTOs = new ArrayList<>();

        for (Post post : posts) {
            postCreationDTOs.add(convert(post));
        }

        return postCreationDTOs;
    }

}

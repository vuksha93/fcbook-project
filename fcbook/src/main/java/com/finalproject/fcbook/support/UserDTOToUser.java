package com.finalproject.fcbook.support;

import java.util.ArrayList;
import java.util.List;

import com.finalproject.fcbook.dto.UserDTO;
import com.finalproject.fcbook.model.About;
import com.finalproject.fcbook.model.User;
import com.finalproject.fcbook.service.AboutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDTOToUser implements Converter<UserDTO, User> {

    @Autowired
    private AboutService aboutService;

    @Override
    public User convert(UserDTO userDTO) {
        User user = new User();

        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        About about = new About();

        about.setId(0);
        about.setEmail(userDTO.getEmail());
        about.setFirstName(userDTO.getFirstName());
        about.setLastName(userDTO.getLastName());
        about.setYearOfBirth(userDTO.getYearOfBirth());

        aboutService.save(about);
        user.setAbout(about);
        about.setUser(user);

        return user;
    }

    public List<User> convert(List<UserDTO> userDTOs) {
        List<User> users = new ArrayList<>();

        for (UserDTO userDTO : userDTOs) {
            users.add(convert(userDTO));
        }

        return users;
    }

}
package com.finalproject.fcbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FcbookApplication {

	public static void main(String[] args) {
		SpringApplication.run(FcbookApplication.class, args);
	}

}

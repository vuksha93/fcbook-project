package com.finalproject.fcbook.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostCreationDTO {

    private int id;
    private String content;
    private LocalDateTime creationTime;
}
package com.finalproject.fcbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private int id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private int yearOfBirth;
    private Boolean isFriend;

}
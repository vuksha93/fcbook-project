package com.finalproject.fcbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationDTO {

    private int id;
    private String username;
    private String password;
    private String repeatedPassword;
    private String email;
    private String firstName;
    private String lastName;
    private int yearOfBirth;

}
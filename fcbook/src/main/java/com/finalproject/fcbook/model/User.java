package com.finalproject.fcbook.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode.Exclude;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private String imgPath;

    @Exclude
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private About about;

    @Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<User> friends = new HashSet<>();

    @Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Post> posts = new ArrayList<>();

    @Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Comment> comments = new ArrayList<>();

    public void addFriend(User user) {
        this.getFriends().add(user);
        user.getFriends().add(this);
    }

    public void unfriend(User user) {
        this.getFriends().remove(user);
        user.getFriends().remove(this);
    }

    public void addPost(Post post) {
        post.setUser(this);
    }

    public void addComment(Comment comment) {
        comment.setUser(this);
    }

}
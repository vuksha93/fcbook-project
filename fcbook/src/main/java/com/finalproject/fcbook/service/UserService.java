package com.finalproject.fcbook.service;

import java.util.List;

import com.finalproject.fcbook.dto.AuthRequest;
import com.finalproject.fcbook.dto.UserDTO;
import com.finalproject.fcbook.model.User;

public interface UserService {

    User findOne(int id);

    User findByUsername(String username);

    List<User> findAll();

    User save(User user);

    User remove(int id);

    String generateToken(AuthRequest authRequest) throws Exception;

    List<UserDTO> getFriendsByUserID(int id, int requestorID);

    List<UserDTO> search(String name, int requestorID);

    UserDTO findUserByID(int id, int requestorID);

}
package com.finalproject.fcbook.service;

import java.util.List;

import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.Post;

public interface PostService {

    Post save(Post post);

    Post findOne(int id);

    Post remove(int id);

    List<Comment> getPostComments(int id);

    List<Post> getFeed(int id);

    List<Post> getUserPosts(int id);

}
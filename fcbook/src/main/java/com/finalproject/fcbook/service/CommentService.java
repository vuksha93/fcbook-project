package com.finalproject.fcbook.service;

import com.finalproject.fcbook.model.Comment;

public interface CommentService {

    Comment save(Comment comment);

    Comment findOne(int id);

    Comment remove(int id);

}
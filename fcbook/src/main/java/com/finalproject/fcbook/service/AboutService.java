package com.finalproject.fcbook.service;

import com.finalproject.fcbook.model.About;

public interface AboutService {

    About save(About about);

}
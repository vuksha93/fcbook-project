package com.finalproject.fcbook.repository;

import java.util.List;

import com.finalproject.fcbook.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

        User findByUsername(String username);

        @Query("SELECT u, CASE WHEN me IS NULL THEN false ELSE true END "
                        + "FROM User u LEFT JOIN u.friends me ON me.id = :requestorId" + " WHERE u.id = :id")
        List<Object[]> findUserById(int id, int requestorId);

        @Query("SELECT f, CASE WHEN me IS NULL THEN false ELSE true END " + "FROM User u JOIN u.friends f "
                        + "LEFT JOIN f.friends me ON me.id = :requestorId " + "WHERE u.id = :id")
        List<Object[]> getFriendsByUserID(int id, int requestorId);

        @Query("SELECT u, CASE WHEN me IS NULL THEN false ELSE true END FROM User u LEFT JOIN u.friends me "
                        + "ON me.id = :requestorId WHERE u.id != :requestorId AND u.about.firstName LIKE %:name% OR "
                        + "u.about.lastName LIKE %:name%")
        List<Object[]> search(String name, int requestorId);

}
package com.finalproject.fcbook.repository;

import java.util.List;

import com.finalproject.fcbook.model.Comment;
import com.finalproject.fcbook.model.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    @Query("SELECT p.comments FROM Post p WHERE p.id = :id")
    List<Comment> getPostComments(int id);

    @Query("SELECT p FROM Post p WHERE p.user.id = :id OR p.user.id IN ("
            + "SELECT f.id FROM User u JOIN u.friends f WHERE u.id = :id" + ") ORDER BY p.creationTime DESC")
    List<Post> getFeed(int id);

    @Query("SELECT p FROM User u JOIN u.posts p WHERE u.id = :id ORDER BY p.creationTime DESC")
    List<Post> getUserPosts(int id);

}